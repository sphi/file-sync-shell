# File Sync Shell

A very basic way to get some of the functionality of a shell when you only have file sync capabilities. Built with shelling into Android running Syncthing in mind. Note that Syncthing is not designed for this, thus is very slow (roundtrips take ~20 seconds).

The server reads input from `exec.json` (in the same directory as the script) and runs whatever shows up in there as shell commands. The client prints it's output from `output.json`. Both clear their file after reading and only write to empty files, so there shouldn't be any conflicts.

## Setup on Android
- Install termux from F-droid
- Go into it's settings and give it filesystem permissions
- Link home dir into termux dir to make things easier to work with (`ln -s /storage/emulated/0 home`)
- Setup Syncthing
- Copy this repo into Syncthing and let it sync to the phone
- In termux run `bash .../termux-setup.sh`
- If you want location access, install Termux:API and in settings give it access to location all the time

## TODO
- Run single commands
- Timeout

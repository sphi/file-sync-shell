#!/bin/python3

import os
import logging
import json
import time
import sys
import subprocess

logging.basicConfig(level=logging.INFO)
quit_line = '[exited]'
exec_f_name = 'exec.json'
output_f_name = 'output.json'
time_f_name = 'server-time.json'
iter_time = 1
timeout_time = 15 * 60
write_every = 15

# Idea behind this protocol is that for each single directional stream there is one file synced between two
# devices. The writer only writes to the file when it's empty, the reader only reads from the file when it's non-empty
# and the reader empties the file when it's done. When the file is non-empty it contains a JSON array of strings.
class ReadFile:
    def __init__(self, path: str) -> None:
        self.path = path

    def read_lines(self) -> list[str]:
        try:
            if os.stat(self.path).st_size:
                with open(self.path, 'r+') as f:
                    lines: list[str] = json.load(f)
                    f.truncate(0)
                    assert isinstance(lines, list), 'not a list (' + repr(lines) + ')'
                    for line in lines:
                        assert isinstance(line, str), 'list contains non-string (' + repr(lines) + ')'
                    return lines
        except FileNotFoundError:
            pass
        except AssertionError as e:
            logging.error('error reading %s: %s', self.path, str(e))
        return []

class WriteFile:
    def __init__(self, path: str) -> None:
        self.path = path
        self.buffer: list[str] = []

    def flush_buffer(self) -> bool:
        '''Returns true if buffer is empty'''
        if self.buffer:
            try:
                if os.stat(self.path).st_size:
                    return False
            except FileNotFoundError:
                pass
            with open(self.path, 'w') as f:
                json.dump(self.buffer, f, indent=2)
                self.buffer = []
        return True

    def blocking_flush(self):
        while not self.flush_buffer():
            time.sleep(iter_time)

    def add_line(self, line: str) -> None:
        assert isinstance(line, str), 'line added to file not a string: ' + repr(line)
        self.buffer.append(line)

class Runner:
    def __init__(self) -> None:
        pass

    def run(self, command: str) -> str:
        logging.info('running command: %s', command)
        result = subprocess.run(command, shell=True, capture_output=True, encoding='utf8', text=True)
        output: list[str] = []
        if result.stdout:
            output.append(result.stdout)
        if result.stderr:
            output.append(result.stderr)
        out_str = '\n'.join(output)
        logging.info(
            'exited with %d lines of output and exit code %d',
            out_str.count('\n'),
            result.returncode)
        return out_str.rstrip()

class Connection:
    def __init__(self) -> None:
        self.main_dir = os.path.dirname(os.path.realpath(__file__))
        self.should_quit = False

    def exec_path(self) -> str:
        return os.path.join(self.main_dir, exec_f_name)

    def output_path(self) -> str:
        return os.path.join(self.main_dir, output_f_name)

    def iteration(self) -> None:
        pass

class Server(Connection):
    def __init__(self) -> None:
        super().__init__()
        logging.info('server created using directory %s', self.main_dir)
        self.read = ReadFile(self.exec_path())
        self.write = WriteFile(self.output_path())
        self.runner = Runner()
        self.time_file_path = os.path.join(self.main_dir, time_f_name)
        current_time = time.time()
        try:
            with open(self.time_file_path, 'r') as f:
                old_time = json.load(f)
                assert current_time - old_time > write_every + 1, 'another server is currently running'
        except FileNotFoundError:
            pass
        self.written_time = 0.0
        self.exec_time = current_time
        self.write_time(current_time)

    def exit(self) -> None:
        self.should_quit = True
        self.write.add_line(quit_line)
        self.write.blocking_flush()
        os.remove(self.time_file_path)

    def write_time(self, current: float) -> None:
        self.written_time = current
        with open(self.time_file_path, 'w') as f:
            json.dump(current, f)

    def iteration(self) -> None:
        current_time = time.time()
        if current_time - self.written_time > write_every:
            self.write_time(current_time)
        for line in self.read.read_lines():
            self.exec_time = current_time
            if line == 'exit':
                self.exit()
                return
            self.write.add_line(self.runner.run(line))
        self.write.flush_buffer()
        if current_time - self.exec_time > timeout_time:
            logging.info('timed out')
            self.exit()

class Client(Connection):
    def __init__(self) -> None:
        super().__init__()
        logging.info('client created using directory %s', self.main_dir)
        self.read = ReadFile(self.output_path())
        self.write = WriteFile(self.exec_path())
        self.inflight = False

    def iteration(self) -> None:
        while True:
            output = self.read.read_lines()
            if output or not self.inflight:
                break
            time.sleep(iter_time)
        self.inflight = False
        for line in output:
            print(line)
            if line.strip() == quit_line:
                self.should_quit = True
                return
        line = input('$ ').strip()
        if line:
            self.inflight = True
            self.write.add_line(line)
            self.write.blocking_flush()

if __name__ == '__main__':
    assert len(sys.argv) == 2, 'should have 1 arg'
    connection: Connection
    arg = sys.argv[1]
    if arg == 'client':
        connection = Client()
    elif arg == 'server':
        connection = Server()
    elif 'help' in arg or arg == '-h':
        print('USAGE: ' + os.path.basename(__file__) + ' [client,server,help]')
        print('uses ' + exec_f_name + ' and ' + output_f_name + ' to sorta emulate a shell')
        print('"useful" for contexts when you only have file sync (like syncthing) to access a device')
        exit(0)
    else:
        assert False, 'arg should be either client, server or help'
    while not connection.should_quit:
        connection.iteration()
        time.sleep(iter_time)
    logging.info('done')

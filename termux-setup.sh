#!/bin/bash
set -euo pipefail

# other useful packages: tsu for sudo, git, termux-api
pkg update
pkg install python3 cronie

if ! which sv-enable; then
  pkg install termux-services
  echo
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo
  echo "termux-serveices installed, YOU MUST NOW RESTART TERMUX AND RUN THE SCRIPT AGAIN!"
  echo
  exit 1
fi

# Directory of script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
rm -f ~/file-sync-shell
ln -sf "$DIR" ~/file-sync-shell

sv-enable crond
crontab ~/file-sync-shell/crontab

echo "Server should be running now"
